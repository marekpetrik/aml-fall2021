# Advanced Machine Learning Fall 2021

The class content is flexible and based on student's preferences. 

## Proposed Schedule


| Date   | Day | Topic                                | Reading     | Presenter |
|--------|-----|--------------------------------------|-------------|-----------|
| Aug 31 | Tue | Introduction and discussion          |             |           |
| Sep 02 | Thu | Stochastic Bandits                   | IMB 1       | Marek     |
| Sep 07 | Tue | Stochastic Bandits                   | IMB 1       | Marek     |
| Sep 09 | Thu | Exploring multi-armed bandits        | IMB 1       | Marek     |
| Sep 14 | Tue | Probability and Markov Chains        | BA 2,3      | Thomas    |
| Sep 16 | Thu | Probability and Markov Chains        | BA 2,3      | Thomas    |
| Sep 21 | Tue | Stochastic Bandits                   | BA 4        | Monkie    |
| Sep 23 | Thu | Concentrations                       | BA 5        | Elita     |
| Sep 28 | Tue | Concentrations                       | BA 5        | Marek     |
| Sep 30 | Thu | Explore-then-commit                  | BA 6        | Connor    |
| Oct 05 | Tue | UCB                                  | BA 7        | Khem      |
| Oct 07 | Thu | Bayesian Bandits                     | IMB 3       | Marek     |
| Oct 12 | Tue | Contextual Bandits                   | BA 18,IMB 8 | Khem      |
| Oct 14 | Thu | Stochastic Linear Bandits            | BA 19       | Thomas    |
| Oct 19 | Tue | Exp3 and Project Idea                | BA 11       | Marek     |
| Oct 21 | Thu | Convex Analysis                      | BA 26       | Monkie    |
| Oct 26 | Tue | Exp3                                 | BA 27       | Connor    |
| Oct 28 | Thu | Mirror Descent                       | BA 28       | Elita     |
| Nov 02 | Tue | Exp3 (no context)                    | BA 11       | Marek     |
| Nov 04 | Thu | Markov Decision Process              | BA 28       | Marek     |
| Nov 09 | Tue | Gittins index                        | BA 35       | Marek     |
| Nov 11 | Thu | *No class*                           |             |           |
| Nov 16 | Tue | Topic / Paper  (see mycourses)       |             |           |
| Nov 18 | Thu | Topic / Paper  (see mycourses)       |             |           |
| Nov 23 | Tue | *No class*                           |             |           |
| Nov 25 | Thu | Topic / Paper  (see mycourses)       |             |           |
| Nov 30 | Tue | Topic / Paper  (see mycourses)       |             |           |
| Dec 02 | Thu | Topic / Paper  (see mycourses)       |             |           |
| Dec 07 | Tue | Topic / Paper  (see mycourses)       |             |           |
| Dec 09 | Thu | Conclusion                           |             |           |


## Main Textbooks

- **BA**: [Lattimore and Szepesvari (2019), Bandit Algorithms](https://tor-lattimore.com/downloads/book/book.pdf)
- **IMB**: [Aleksandrs Slivkins (2019), Introduction to Multi-armed Bandits](https://arxiv.org/pdf/1904.07272.pdf)
- **CI**: Boucheron, S., Lugosi, G., & Massart, P. (2013). Concentration Inequalities: A Nonasymptotic Theory of Independence. Oxford University Press.

## Office Hours

- Thu 1-2pm in Kingsbury N215b or [https://unh.zoom.us/j/5833938643](https://unh.zoom.us/j/5833938643) (email if using zoom)
- Tue 8:30-9:30am in Kingsbury N215b or [https://unh.zoom.us/j/5833938643](https://unh.zoom.us/j/5833938643) (email if using zoom)

#### Paper Assignments

TBD. We will have a poll to choose which papers to read. Good places to look for papers are: ICML, NeurIPS/NIPS, JMLR, JAIR, Operations Reseach, Math of Operations Research, IEEE TAC, EJOR

## Grading ##

- Topic & paper presentation  (40%)
- Project  (40%)
- Paper response (20%)  (pass fail)

## Presenting a Textbook Topic ##

Each student is expected to present about 2-4 topics. The presentation should aim for clarity and the presenter should be able to answer basic questions about the topic.

## Presenting a Paper ##

Every student is expected to present 1-3 papers. Please review the following helpful guide on how to review read research papers. [How to read a research paper](docs/efficientReading.pdf)

When presenting a paper or writing a response to it, you should address the following points:

1. *Motivation*: Why is this problem important?
2. *Challenge*: Why is it difficult?
3. *Approach*: What is the main approach?
4. *Novelty*: What is new about the paper?
5. *Evidence*: Is there sufficient evidence of the effectiveness of the methods?
6. *Limitations*: What is it that the paper does not address? Are there alternative methods that can work?

Please prepare slides for the presentation that include relevant figures or tables from the paper.


## Writing a Paper Response ##

For every paper (column reading says: "paper"), every student is expected to write a short paper response. The response should consist of:

1. A paragraph describing the main idea of the paper
2. 2-4 paragraphs addressing the points mentioned in the section "Presenting a Paper"

## Project ##

The project is a group project with group size of 1 - 5. I will offer several options or you may choose a topic that interests you.
