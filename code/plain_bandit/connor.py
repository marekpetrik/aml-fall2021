import random
from math import log2, floor
from statistics import mean

import numpy as np
from scipy.stats import bernoulli
from matplotlib import pyplot as plt


# Number of arms, number of rounds
K = 5
T = 100

# Set this flag to randomize the D_i values
D_random = False
# Preset D_i values
D = [0.23, 0.51, 0.83, 0.45, 0.70]


def pull(d_i):
    """Pull an arm with D_i value d_i"""
    return bernoulli.rvs(d_i)


def regret(pulled, optimal):
    """Calculate the regret of a pull given the optimal distribution parameter and the pulled arm"""
    return optimal - pulled


def run_explore_first(k, t, d):
    """Do a full run of the explore first algorithm with provided parameters"""
    # Calculated number of explore sweeps
    n = floor(((t / k) ** (2/3)) * (log2(t) ** (1/3)))

    # Precompute the expected value of the best arm for regret analysis
    d_star = max(d)

    # Calculate explore rounds
    explore_rewards = [[pull(d_i) for _ in range(n)] for d_i in d]
    explore_means = [mean(rewards) for rewards in explore_rewards]
    # After calculating the means for each arm, flatten the explore rewards
    explore_rewards = [e for er in explore_rewards for e in er]

    # Calculate regret for each pull during the exploration phase
    explore_regrets = [regret(d_i, d_star) for d_i in d for _ in range(n)]

    # Exploitation phase: find best arm from exploration, calculate rewards and regrets
    exploit_rounds = t - (n * k)
    best_found_arm = np.argmax(explore_means)
    exploit_rewards = [pull(d[best_found_arm]) for _ in range(exploit_rounds)]
    exploit_regrets = [regret(d[best_found_arm], d_star) for _ in range(exploit_rounds)]

    # Concatenate phases
    rewards = explore_rewards + exploit_rewards
    regrets = explore_regrets + exploit_regrets
    cumulative_reward = np.cumsum(rewards)
    cumulative_regret = np.cumsum(regrets)

    return cumulative_regret


xs = np.arange(0, T)
N = floor(((T / K) ** (2/3)) * (log2(T) ** (1/3)))

# 1. sample + plot pseudo-regret for single D_i parameters
p1_regrets = run_explore_first(K, T, D)


plt.figure(0)
plt.plot(xs, p1_regrets)
plt.suptitle("Pseudo-Regret for Fixed D_i")
plt.xlabel("Pull")
plt.ylabel("Regret")
plt.axvline(N*K, ls='--', c='purple')
plt.grid()

# 2. Compute expected regret over 1000 runs for fixed D_i
p2_regrets = np.array([run_explore_first(K, T, D) for _ in range(1000)])
p2_regret_means = np.mean(p2_regrets, 0)

plt.figure(1)
plt.plot(xs, p2_regret_means)
plt.suptitle("Expected Regret for Fixed D_i")
plt.xlabel("Pull")
plt.ylabel("Regret")
plt.axvline(N*K, ls='--', c='purple')
plt.grid()

# 3. Compute expected regret over 100 possible D_i values
p3_regrets = np.array([run_explore_first(K, T, [random.random() for _ in range(K)]) for _ in range(100)])
p3_regret_means = np.mean(p3_regrets, 0)

plt.figure(2)
plt.plot(xs, p3_regret_means)
plt.suptitle("Expected Regret for Variable D_i")
plt.xlabel("Pull")
plt.ylabel("Regret")
plt.axvline(N*K, ls='--', c='purple')
plt.grid()

plt.show()
