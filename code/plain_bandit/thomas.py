import math
import matplotlib.pyplot as plt
from random import *
from scipy.stats import bernoulli
import numpy as np

class Arm:
    mean = 0
    total = 0
    pulled = 0

    def __init__(self, mean):
        self.mean = mean

    def getAverage(self):
        if(self.pulled == 0):
            return self.total
        else:
            return self.total/self.pulled

    def pull(self):
        value = bernoulli.rvs(self.mean)
        self.total += value
        self.pulled += 1
        return value

def exploit(arms, k):
    best = 0
    bestAvg = 0
    currAvg = 0
    for i in range(0,k):
        currAvg = arms[i].getAverage()
        if(currAvg > bestAvg):
            best = i
            bestAvg = currAvg
    return best

def getOptimal(arms, k, t):
    bestMean = 0
    for i in range(0,k):
        if(arms[i].mean > bestMean):
            bestMean = arms[i].mean
    return bestMean * t

def armMeanSum(arms, armhistory, rounds):
    total = 0
    for i in range(0,rounds):
        total += arms[armhistory[i]].mean
    return total

def greedy():
    #Configure
    rounds = 100
    k = 3 #Max arms = 10 for fixed distributions

    arms = []
    armHistory = []
    armMeans = [0.7,0.5,0.12,0.34,0.87,0.34,0.12,0.76,0.91,0.65]
    reward = 0
    optimal = 0
    pseudoRegret = 0
    pseudoRegrets = []

    #load array of arm objects with random mean value
    for i in range(0,k):
        newArm = Arm(mean=armMeans[i]) #mean=random()
        arms.append(newArm)

    #run greedy algorithm
    for i in range(1,rounds+1):
        prob = math.pow(i,-1/3) * math.pow(k*math.log(i),1/3)
        if(random() <= prob):
            #explore
            curr = randint(0,k-1)
            reward += arms[curr].pull()
            armHistory.append(curr)
        else:
            #exploit
            curr = exploit(arms, k)
            reward += arms[curr].pull()
            armHistory.append(curr)
        optimal = getOptimal(arms, k, i)
        pseudoRegrets.append(optimal - armMeanSum(arms, armHistory, i))

    optimal = getOptimal(arms, k, rounds)
    armMeanSums = armMeanSum(arms, armHistory, rounds)
    pseudoRegret = optimal - armMeanSums
    return pseudoRegrets

rounds = 100
totalj = 0
totali = 0
pseudoRegrets = []
pseudoRegretAvgs = []
for i in range(1,rounds):
    pseudoRegrets.append(greedy())
#for i in range(0,rounds-1):
#    for j in range(0,rounds-1):
#        totalj += pseudoRegrets[j][i]
#    pseudoRegretAvgs.append(totalj/rounds)
pseudoRegretsAvgs = np.mean(pseudoRegrets, 0)

#Problem 1
plt.plot(greedy())
plt.xlabel("Round")
plt.ylabel("Pseudoregret")
plt.show()

#Problem 2
plt.plot(pseudoRegretAvgs)
plt.xlabel("Round")
plt.ylabel("Expected Regret")
plt.show()

# #Problem 3
# I did not have time to finish this and was a unclear what we mean by worst case, a couple of possibilities are below:
# 1. That we never get to exploit in which case we have a uniform distribution of arm pulls hence linear regret
# 2. That our exploit choice is never the optimal choice in which case we get a linear regret (i.e. the guessed mean for an arm is
# larger than the guessed mean for the optimal arm)
# 3. We never get to explore the optimal arm

# #Test Output
# print("Optimal:")
# print(optimal)
# print("Mean Sums:")
# print(armMeanSums)
# print("Actual Reward:")
# print(reward)
# print("Pseudo Regret:")
# print(pseudoRegret)