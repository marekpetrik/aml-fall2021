#############################################
#
# python3 successive_elim.py --help
## Experiment 1
# python3 successive_elim.py --exp=1 
## Experiment 2
# python3 successive_elim.py --exp=2 --iter=100
## Experiment 3
# python3 successive_elim.py --exp=3 --iter=100
# python3 successive_elim.py  --exp=3  --iter 100 --ds=-1  --k=3 --t=4000
#
# By Khem Veasna
#
#############################################

import time
import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
import copy
from operator import itemgetter

parser = OptionParser()
parser.add_option("--debug", action="store_true", dest="debug", help="debug")
#parser.add_option("--p", type="float", nargs="+", dest="probs")
parser.add_option("--k", metavar="NUMBER", default=3, type="int", dest="K", help="K")
parser.add_option("--t", metavar="NUMBER", default=1000, type="int", dest="T", help="T")
parser.add_option("--iter", metavar="NUMBER", default=10, type="int", dest="iter", help="Number of iterations")
parser.add_option("--exp", metavar="NUMBER", default=1, type="int", dest="exp", help="Which experiment: 1, 2 or 3")
parser.add_option("--ds", metavar="NUMBER", default=-1, type="int", dest="ds", help="downsample factor")
parser.add_option("--disableplot", default=True, action = 'store_false', dest="plot", help="")

(options, args) = parser.parse_args()


# Initialize the arms
def init_arms(num_of_arms):
    list_of_arms = []

    for i in range(1, num_of_arms+1):
        name = "Arm%d" % (i)
        new_arm = { 'Name': name, 'Num': i, 'P':1, 'Active': True , 'Rewards': [], 'Related_T': []}
        list_of_arms.append(new_arm)

    return list_of_arms

def count_active_arm(arms):
    count = 0
    for arm in arms:
        if arm['Active']:
            count += 1

    return count

# Compute the radius
def rad(state, arm, t):
    nt = len(arm['Rewards'])
    found_t_idx = reward_idx_to_last_round_played(arm, t)
    # idx of 0 means round 1, so shift by 1
    num_of_rounds_played_by_arm = found_t_idx + 1
    r = np.sqrt(2*np.log(state['T'])/num_of_rounds_played_by_arm)
    return r

def ucb(state, arm, t):
    mean_u = cur_avg_reward_with_t(arm, t)
    return mean_u + rad(state, arm, t)

def lcb(state, arm, t):
    mean_u = cur_avg_reward_with_t(arm, t)
    return mean_u - rad(state, arm, t) 

def sample_from_iid_arm(p):
    return np.random.binomial(1, p);

def pull_arm(arm, round):
    num = arm['Num']
    p = arm['P']

    r = sample_from_iid_arm(p)
    arm['Rewards'].append(r)

    # Track at which round the arm was pulled
    arm['Related_T'].append(round)

def reward_idx_to_last_round_played(arm, up_to_t):
    rewards = arm['Rewards']

    idx_to_last_round = 0
    related_t = arm['Related_T']
    for i in range(0, len(related_t)):
        if related_t[i] <= up_to_t: 
            idx_to_last_round = i
        else:
            break
    return idx_to_last_round

def cur_avg_reward_with_t(arm, up_to_t):
    rewards = arm['Rewards']

    idx_to_last_round = reward_idx_to_last_round_played(arm, up_to_t)

    if idx_to_last_round == 0:
        return 0

    return np.mean(rewards[0:idx_to_last_round])

def inc_round(state):
    r = state['Cur_Round']
    state['Cur_Round'] = r + 1

def should_run(state):
    r = state['Cur_Round']
    if r >= state['T']:
        return False
    else:
        return True

def get_arm(arms, num):
    for arm in arms:
        if arm['Num'] == num:
            return arm
    return None

def compute_regret(arms, t, Total_Rounds, track_arm_actions):
    best_arm = arms[0]
    for arm in arms:
        # Get the best arm
        if arm['P'] > best_arm['P']:
            best_arm = arm

    arm_num = track_arm_actions[t]
    arm = get_arm(arms, arm_num)

    d = best_arm['P'] - arm['P']
    assert (abs(d) <= 1), "d needs to be between 0 and 1"
    return d

def s_elim(state, arms):
    track_arm_actions = []

    if options.debug:
        print("State is: %s" % (state))

    phase = 1
    if options.debug:
        print("Calculate s_elim")
    while True:        
        if state['Cur_Round'] >= state['T']:
            break
        
        # Pull each arm
        for arm in arms:
            if arm['Active']:
                if should_run(state):
                    round =  state['Cur_Round'] + 1
                    pull_arm(arm, round)
                    # Track arms pulled
                    track_arm_actions.append(arm['Num'])
                    inc_round(state)

        # Check bounds
        for arm1 in arms:
            for arm2 in arms:
                if arm1['Num'] == arm2['Num']:
                    continue
                lcbt = lcb(state, arm1, state['Cur_Round'])
                ucbt = ucb(state, arm2, state['Cur_Round'])

                if arm1['Active'] and arm2['Active']:
                    if ucbt < lcbt:
                        print("%s: Round %d: Got UCB_t(a) < LCB_t(a*): a=%s, a*=%s (%f < %f)" % (state['Name'], state['Cur_Round'], arm2['Name'], arm1['Name'], ucbt, lcbt))                        
                        print("  %s will be deactivated." % (arm2['Name']))
                        arm2["Active"] = False
                        print("  %d out of %d arms active." % (count_active_arm(arms), len(arms)))
        phase +=1 
    if options.debug:
        print("Done calculating s_elim")
    return track_arm_actions

def downsample_time(T):
    N = options.ds
    dat = []
    shift = 1

    if N > 0:
        shift = np.floor(T/N)

    j = 0
    while j < T:
        dat.append(j) 
        j = j + shift
    return dat

def downsample_data(data):
    N = options.ds
    d = []
    length = len(data)

    shift = 1
    if N > 0:
        shift = np.floor(length/N)    

    j = 0
    while j < length:
        d.append(data[j])
        j = j + int(shift)
    return d

def experiment_one(state, arms):
    if options.debug:
        for a in arms:
            print(a)

    track_arm_actions = s_elim(state, arms)

    if options.debug:
        num_of_active_arms = 0
        for arm in arms:
            print("Arm %d is Active: %s" % (arm['Num'], arm['Active']))
            if arm['Active']:
                num_of_active_arms += 1

        print(state)
        print("Number of active arms %d" % (num_of_active_arms))
        print(track_arm_actions[0:100])
        print(track_arm_actions[-100:-1])

    if options.plot:
        # Compute regret
        time = []
        T = state['T']
        regret = []

        ds_time = downsample_time(T)

        for t in ds_time:
            time.append(t)

        for t in range(0, T):
            v = compute_regret(arms, t, T, track_arm_actions)
            regret.append(v)

        cum_regret = np.cumsum(regret)
        downsampled_cum_regret = downsample_data(cum_regret)

        print("Regret....")
        print(regret[0:20])

        print("Cumulative...")
        print(cum_regret[0:20])

        plt.plot(time, downsampled_cum_regret)
        plt.xlabel('Round')
        plt.ylabel('Pseudo-regret')
        probs = list(map(itemgetter('P'), arms))
        active_arm_count = count_active_arm(arms)
        rounded_probs = list(map(lambda x: round(x,2), probs))
        rounded_probs.sort(reverse=True)
        title = "Cumulative regret\n(Active arm count %d out of %d : %s)" % (active_arm_count, len(arms), rounded_probs)
        plt.title(title)
        plt.show()

def experiment_two(cur_state, cur_arms):
    print("Running experiment 2 %d" % ( options.iter))
    print(cur_state)
    tracker = []
    for i in range(0, options.iter - 1):
        arms = copy.deepcopy(cur_arms)
        state = copy.deepcopy(cur_state)
        state['Name'] = "Run %d" % (i)

        run = {'arms': arms, 'state': state}

        track_arm_actions = s_elim(state, arms)
        run['track_arm_actions'] = track_arm_actions
        tracker.append(run)
    
    # Compute regret
    print("Tracker size %d" % (len(tracker)))
    time = []
    
    ds_time = downsample_time(T)
    for t in ds_time:
        time.append(t)

    y_bound = []
    regret = []

    for t in range(0, T):
        # Get data for each arm
        regret_t = 0.0
        for m in tracker:
            arms = m['arms']
            state = m['state']
            track_arm_actions = m['track_arm_actions']
            regret_t += compute_regret(arms, t, state['T'], track_arm_actions)
        regret.append(regret_t/len(tracker))
        y_bound.append(np.sqrt(t*np.log(T)))

    cum_regret = np.cumsum(regret)
    downsampled_cum_regret = downsample_data(cum_regret)

    plt.plot(time, downsampled_cum_regret, label="average pseudo-regret")
    #plt.plot(time, y_bound, label="O-Bound")
    plt.xlabel('Round')
    plt.ylabel('Pseudo-regret')
    title = "Expected regret (%d iterations)" % (options.iter)
    plt.title(title)
    #plt.legend()
    plt.show()

def experiment_three(cur_state, cur_arms):
    print("Running experiment 3 %d" % ( options.iter))
    tracker = []
    for i in range(0, options.iter - 1):
        arms = copy.deepcopy(cur_arms)
        for arm in arms:
            # Set the distribution for each arm
            arm['P'] = np.random.uniform(0.001, 0.8, 1)[0]
        state = copy.deepcopy(cur_state)
        state['Name'] = "Run %d" % (i)

        run = {'arms': arms, 'state': state}

        track_arm_actions = s_elim(state, arms)
        run['track_arm_actions'] = track_arm_actions
        tracker.append(run)
    
    # Compute regret
    print("Tracker size %d" % (len(tracker)))
    time = []
    
    ds_time = downsample_time(T)
    for t in ds_time:
        time.append(t)

    y = []
    y_bound = []
    regret = []
    regret_indiv = []
    for t in range(0, T):
        # Get data for each arm
        cum_regret_t = 0.0
        regrets_t = []
        for m in tracker:
            arms = m['arms']
            state = m['state']
            track_arm_actions = m['track_arm_actions']
            r = compute_regret(arms, t, state['T'], track_arm_actions)
            cum_regret_t += r
            
            regrets_t.append(r)
        regret.append(cum_regret_t/len(tracker))
        regret_indiv.append(regrets_t)
        y_bound.append(np.sqrt(t*np.log(T)))

    cum_regret = np.cumsum(regret)
    downsampled_cum_regret = downsample_data(cum_regret)

    downsampled_y_bound = downsample_data(y_bound)

    # break up the regrets for separate runs
    cum_regrets = []

    k = regret_indiv[0]
    num_of_runs = len(k)
    for i in range(0, num_of_runs):
         cum_regrets.append(list(map(lambda x: x[i], regret_indiv)))

    #plt.plot(time, downsampled_cum_regret, label="average worst regret")
    plt.plot(time, downsampled_y_bound, label="big O", linewidth=4)

    for c in cum_regrets:
        cum_c = np.cumsum(c)
        downsampled_cum_c = downsample_data(cum_c)
        plt.plot(time, downsampled_cum_c)

    plt.xlabel('Round')
    plt.ylabel('Pseudo-regret')
    plt.title('(worst) regret')
    plt.legend()
    plt.show()

def experiment_four(state, arms):
    for arm in arms:            
        arm['P'] = np.random.uniform(0, 1, 1)[0]
        print(arm)

    track_arm_actions = s_elim(state, arms)

    if options.debug:
        num_of_active_arms = 0
        for arm in arms:
            print("Arm %d is Active: %s" % (arm['Num'], arm['Active']))
            if arm['Active']:
                num_of_active_arms += 1

        print(state)
        print("Number of active arms %d" % (num_of_active_arms))
        print(track_arm_actions[0:100])
        print(track_arm_actions[-100:-1])

    if options.plot:
        # Compute regret
        time = []
        T = state['T']
        regret = []

        ds_time = downsample_time(T)

        for t in ds_time:
            time.append(t)

        for t in range(0, T):
            v = compute_regret(arms, t, T, track_arm_actions)
            regret.append(v)

        cum_regret = np.cumsum(regret)
        downsampled_cum_regret = downsample_data(cum_regret)

        print("Regret....")
        print(regret[0:20])

        print("Cumulative...")
        print(cum_regret[0:20])

        plt.plot(time, downsampled_cum_regret)
        plt.xlabel('Round')
        plt.ylabel('Pseudo-regret')
        probs = list(map(itemgetter('P'), arms))
        active_arm_count = count_active_arm(arms)
        rounded_probs = list(map(lambda x: round(x,2), probs))
        rounded_probs.sort(reverse=True)
        title = "Cumulative regret\n(Active arm count %d out of %d : %s)" % (active_arm_count, len(arms), rounded_probs)
        plt.title(title)
        plt.show()


if __name__ == '__main__':
    K = int(options.K)
    T = int(options.T)

    state = {'Name':'', 'Round': 1, 'K': K, 'T': T, 'Cur_Phase': 0, 'Cur_Round': 0}

    arms = init_arms(K)

    # Don't change this....
    np.random.seed(11)
    ps = np.random.uniform(0, 1, K)

    # Experiment 1: fix distributions
    #chosen_ps = [0.7, 0.5, 0.25, 0.24, 0.2, 0.11, 0.1]
    for i in range(0, K):
        arm = arms[i]
        arm['P'] = ps[i]

    t = 1000 * time.time() # current time in milliseconds
    np.random.seed(int(t) % 2**32)

    if options.exp == 1:
        experiment_one(state, arms)
    elif options.exp == 2:
        experiment_two(state, arms)
    elif options.exp == 3:
        experiment_three(state, arms)
    elif options.exp == 4:
        experiment_four(state, arms)

    print("Done!")




