# This is a modification of Connor's code
from typing import *
from policy import *
import numpy as np
import random
from ucb import *
from optparse import OptionParser
import config
import shelve

class AdversarialInstance:
    def __init__(self, rewards: List[List[int]]) -> None:
        self.rewards = rewards
        # Regret comparison defined as selecting a single best arm
        self.regret_comp = max([sum(rs) for rs in self.rewards])

    def pull(self, arm: int, t: int):
        return self.rewards[arm][t]

    def __repr__(self) -> str:
        return self.rewards.__repr__()


def generate_all_instances(arms: int, horizon: int):
    # Produce a generator that iterates through all combinations of 0/1 rewards
    # 2^(k*t) possibilities !! maybe restrict this?

    # Start by constructing the environment itself:
    env = [[0 for _ in range(horizon)] for _ in range(arms)]

    def _dfs(env, a, h):
        # Base case:
        if a == arms-1 and h == horizon-1:
            env[a][h] = 0
            yield AdversarialInstance(env)
            env[a][h] = 1
            yield AdversarialInstance(env)
        # Recursive case:
        else:
            next_h = (h + 1) % horizon
            next_a = a + (1 if h + 1 == horizon else 0)
            env[a][h] = 0
            yield from _dfs(env, next_a, next_h)
            env[a][h] = 1
            yield from _dfs(env, next_a, next_h)

    return _dfs(env, 0, 0)

# Runs the instance using the provided policy
def run_instance(instance, policy: Policy)->None:
    if config.options.debug:
        print("Running instance using: " + str(instance))
    rewards = policy.run(instance)
    regret = policy.compute_regret(rewards)
    exp_rew = np.mean(rewards)
    if config.options.debug:
        print("Payout is: " + str(rewards))
        print("Regret is: " + str(regret))
        print(str(policy) + "Expected rewards: " + str(exp_rew))
    #return regret
    return exp_rew

# Test all instance generation
if __name__ == '__main__':
    test_a = 2 # Arms
    test_h = 9 # horizon

    policy_opt = None
    policy_opt1 = None
    policy_opt2 = None
    policy = None

    #track_regrets_opt = []
    #track_regrets = []

    track_exp_rewards_opt = []
    track_exp_rewards = []
    track_exp_rewards_opt1 = []
    track_exp_rewards_opt2 = []

    track_exp_beliefs = []
    for instance in generate_all_instances(test_a, test_h):
        policy_opt = UCB()
        policy_opt1 = Durpy(0)
        policy_opt2 = Durpy(1)
        policy = ETC(2)

        if config.options.debug:
            print(instance)
            print(type(instance))

        # For each instance...
        # Run forward while maintainting states/beliefs
        exp_reg_opt = run_instance(instance, policy_opt)
        track_exp_rewards_opt.append(exp_reg_opt)
        
        #import pdb; pdb.set_trace()

        exp_reg = run_instance(instance, policy)
        track_exp_rewards.append(exp_reg)
        track_exp_beliefs.append(policy.beliefs)

        # Run durpy
        exp_reg_opt1 = run_instance(instance, policy_opt1)
        track_exp_rewards_opt1.append(exp_reg_opt1)

        exp_reg_opt2 = run_instance(instance, policy_opt2)
        track_exp_rewards_opt2.append(exp_reg_opt2)
        
    #print(track_regrets_opt)
    #print(max(track_regrets_opt))
    #print(track_exp_rewards)

    if config.options.debug:
        print(track_exp_rewards_opt[175:200])
        print(track_exp_rewards[175:200])
        print(track_exp_rewards_opt1[175:200])

    vector1 = np.array(track_exp_rewards_opt)
    vector2 = np.array(track_exp_rewards_opt1)
    vector3 = np.array(track_exp_rewards_opt2)
    vector_null = np.array(track_exp_rewards)

    diff1 = vector1 - vector_null
    diff2 = vector2 - vector_null
    diff3 = vector3 - vector_null

    #import pdb; pdb.set_trace()

    print("Eval (UCB & ETC): " + str(max(diff1)))
    print("Eval (Durpy0 & ETC): " + str(max(diff2)))
    print("Eval (Durpy1 & ETC): " + str(max(diff3)))

    # Serialize some data to a file, so that we can analyze it later
    #d = shelve.open('my_data')
    #d['track_exp_beliefs'] = track_exp_beliefs
    #d['track_exp_rewards_opt'] = track_exp_rewards_opt
    #d['track_exp_rewards'] = track_exp_rewards
    #d['track_exp_rewards_opt1'] = track_exp_rewards_opt1
    #d['track_exp_rewards_opt2'] = track_exp_rewards_opt2
    #d.close()
    
