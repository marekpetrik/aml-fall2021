from policy import *
import numpy as np
import config

class UCB(Policy):
    def __init__(self):
        Policy.__init__(self)

    # Initialize the arms
    def init_arms(self, num_of_arms):
        list_of_arms = []

        for i in range(1, num_of_arms+1):
            name = "Arm%d" % (i)
            new_arm = { 'Name': name, 'Num': i, 'P':1, 'Active': True , 'Rewards': [], 'Related_T': []}
            list_of_arms.append(new_arm)

        return list_of_arms

    def count_active_arm(self, arms):
        count = 0
        for arm in arms:
            if arm['Active']:
                count += 1

        return count

    # Compute the radius
    def rad(self, state, arm, t):
        nt = len(arm['Rewards'])
        found_t_idx = self.reward_idx_to_last_round_played(arm, t)
        # idx of 0 means round 1, so shift by 1
        num_of_rounds_played_by_arm = found_t_idx + 1
        r = np.sqrt(2*np.log(state['T'])/num_of_rounds_played_by_arm)
        return r

    def ucb(self, state, arm, t):
        mean_u = self.cur_avg_reward_with_t(arm, t)
        return mean_u + self.rad(state, arm, t)

    def lcb(self, state, arm, t):
        mean_u = self.cur_avg_reward_with_t(arm, t)
        return mean_u - self.rad(state, arm, t) 

    def sample_from_iid_arm(self, p):
        return np.random.binomial(1, p);

    def pull_arm(self, arm, round):
        num = arm['Num']
        p = arm['P']


        r = -1
        
        if num == 1:
            r = (self.instance.rewards[0])[round-1]
        elif num == 2:
            r = (self.instance.rewards[1])[round-1]
        else:
            raise Exception("What?")
        arm['Rewards'].append(r)

        # Track at which round the arm was pulled
        arm['Related_T'].append(round)

    def cur_avg_reward_with_t(self, arm, up_to_t):
        rewards = arm['Rewards']

        idx_to_last_round = self.reward_idx_to_last_round_played(arm, up_to_t)

        if idx_to_last_round == 0:
            return 0

        return np.mean(rewards[0:idx_to_last_round])

    def reward_idx_to_last_round_played(self, arm, up_to_t):
        rewards = arm['Rewards']

        idx_to_last_round = 0
        related_t = arm['Related_T']
        for i in range(0, len(related_t)):
            if related_t[i] <= up_to_t: 
                idx_to_last_round = i
            else:
                break
        return idx_to_last_round

    def inc_round(self, state):
        r = state['Cur_Round']
        state['Cur_Round'] = r + 1

    def should_run(self, state):
        r = state['Cur_Round']
        if r >= state['T']:
            return False
        else:
            return True

    def get_arm(self, arms, num):
        for arm in arms:
            if arm['Num'] == num:
                return arm
        return None


    def compute_regret(self, rewards):
        pass

    def s_elim(self, state, arms):
        track_arm_actions = []

        if config.options.debug:
            print("State is: %s" % (state))

        phase = 1

        while True:        
            if state['Cur_Round'] >= state['T']:
                break
            
            # Pull each arm
            for arm in arms:
                if arm['Active']:
                    if self.should_run(state):
                        round =  state['Cur_Round'] + 1
                        self.pull_arm(arm, round)
                        # Track arms pulled
                        track_arm_actions.append(arm['Num'])
                        self.inc_round(state)

            # Check bounds
            for arm1 in arms:
                for arm2 in arms:
                    if arm1['Num'] == arm2['Num']:
                        continue
                    lcbt = self.lcb(state, arm1, state['Cur_Round'])
                    ucbt = self.ucb(state, arm2, state['Cur_Round'])

                    if arm1['Active'] and arm2['Active']:
                        if ucbt < lcbt:
                            if config.options.debug:
                                print("%s: Round %d: Got UCB_t(a) < LCB_t(a*): a=%s, a*=%s (%f < %f)" % (state['Name'], state['Cur_Round'], arm2['Name'], arm1['Name'], ucbt, lcbt))                        
                                print("  %s will be deactivated." % (arm2['Name']))
                            arm2["Active"] = False
                            if config.options.debug:
                                print("  %d out of %d arms active." % (self.count_active_arm(arms), len(arms)))
            phase +=1 

        return track_arm_actions

    def run(self, instance):
        K = 2
        self.instance = instance
        T = len(instance.rewards[0])
        state = {'Name':'', 'Round': 1, 'K': K, 'T': T, 'Cur_Phase': 0, 'Cur_Round': 0}
        arms = self.init_arms(K)
        track_arm_actions = self.s_elim(state, arms)

        T = state['T']
        # Collect rewards
        collect_rewards = []
        for j in range(0,T):
            arm_num = track_arm_actions[j]

            rew = arms[arm_num-1]['Rewards'].pop(0)
            collect_rewards.append(rew)

        return collect_rewards




