
from state import *
import config
from typing import *

class Belief:
    def __init__(self, round: int)-> None:
        self.round = round
        
    def known(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta

    def __repr__(self) -> str:
        return [self.round, self.alpha, self.beta].__repr__()