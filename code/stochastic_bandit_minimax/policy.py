from abc import ABC, abstractmethod
import config
from belief import *
import scipy as ss
from typing import *

class Policy(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def run(self, instance):
        pass

    def compute_regret(self, rewards):
        pass

class Durpy(Policy):
    def __init__(self, which_arm):
        Policy.__init__(self)
        self.which_arm = which_arm

    def run(self, instance):
        if config.options.debug:
            print("I'm a Durpy policy: " + str(instance))

        # Just pull one arm
        arm_to_pull = 0
        return instance.rewards[self.which_arm]

# Explore then Commit
class ETC(Policy):
    def __init__(self, m):
        Policy.__init__(self)
        self.m = m # exploration
        self.k = 2 # 2 arms
        self.best_arm = None
        self.beliefs = []

    def compute_regret(self, rewards):
        #reg = len(rewards)*self.best_arm_mean - sum(rewards)
        if config.options.debug:
            print("Best arm mean: " + str(self.best_arm_mean))
        reg = len(rewards)*self.best_arm_mean - sum(rewards)
        #reg = len(rewards)*self.best_arm_mean - sum(rewards)/float(len(rewards))
        #print(len(rewards)*self.best_arm_mean)
        #print(sum(rewards))
        #print(len(rewards))
        #print(sum(rewards)/float(len(rewards)))
        return reg

    def run(self, instance):
        if config.options.debug:
            print("I'm an ETC: " + str(instance))

        # Find the true best arm from the instance or env
        a1 = sum(instance.rewards[0])/len(instance.rewards[0])
        a2 = sum(instance.rewards[1])/len(instance.rewards[1])

        if a1 >= a2:
            self.best_arm_mean = a1
        else:
            self.best_arm_mean = a2

        horizon = len(instance.rewards[0])
        if self.m*self.k >= horizon:
            raise Exception("Uh oh")
        track_rewards = []
        arm1_est = 0
        arm2_est = 0

        arm1_play = []
        arm2_play = []
        for j in range(self.m*self.k):
            # Exploration phase  
            rew = instance.pull(j % 2, j)
            track_rewards.append(rew)

            if j % 2 == 0:
                arm1_play.append(rew)
            else:
                arm2_play.append(rew)

        # Time to explore
        arm1_est = sum(arm1_play)/float(len(arm1_play))
        arm2_est = sum(arm2_play)/float(len(arm2_play))

        best_arm = 1
        if arm1_est < arm2_est:
            best_arm = 2

        b = Belief(self.m*self.k-1)    
        b.known(arm1_est, arm2_est)
        self.beliefs.append(b)

        exploited_arm_rewards = []
        for j in range(self.m*self.k, horizon):
            rew = instance.pull(best_arm - 1, j) # 0-index arm
            track_rewards.append(rew)
            exploited_arm_rewards.append(rew)

            b = Belief(j)
            if best_arm == 1:
                new_est = (sum(arm1_play) + sum(exploited_arm_rewards))/(len(arm1_play)  + len(exploited_arm_rewards))
                b.known(new_est, arm2_est)
            else:
                new_est = (sum(arm2_play) + sum(exploited_arm_rewards))/(len(arm1_play)  + len(exploited_arm_rewards))
                b.known(arm1_est, new_est)

            self.beliefs.append(b)
        
        #import pdb; pdb.set_trace()

        return track_rewards
        