from optparse import OptionParser

parser = OptionParser()
parser.add_option("--debug", action="store_true", dest="debug", help="debug")

(options, args) = parser.parse_args()