from worstcase_dynamicprogram import AdversarialDynamicProgram
from stochastic_bruteforce import StochasticBayesianTree
from dynamic_bruteforce import BayesianTree
from tom_sampling import TomSampler


HORIZON = 40
ARMS = 2


class SplitPolicy:
    def __init__(self, t, k) -> None:
        self.horizon = t
        self.arms = k

    def get_probs(self, prior):
        return [1 / self.arms for _ in range(self.arms)]


algos = [
    SplitPolicy(t=HORIZON, k=ARMS),
    BayesianTree(t=HORIZON, k=ARMS),
    StochasticBayesianTree(t=HORIZON, k=ARMS),
    TomSampler(),
]

labels = [
    "Split Policy (50/50)",
    "Deterministic Bayesian Tree",
    "Stochastic Bayesian Tree",
    "Tom Sampling"
]

for (algo, label) in zip(algos, labels):
    adversary = AdversarialDynamicProgram()

    print(f"{label}: {adversary.expected_value(algo, ARMS, HORIZON)}")
