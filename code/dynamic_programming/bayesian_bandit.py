from typing import List, Dict, Tuple
import random

from bayesian_util import BetaPrior, construct_posterior
from dynamic_bruteforce import BayesianTree
from stochastic_bruteforce import StochasticBayesianTree


def bernoulli_pull(p):
    return random.uniform(0, 1) < p


def run_bandit(bandit_instance: List[float], horizon: int, policy):
    reward = 0
    regret_comp = max(bandit_instance) * horizon
    prior = tuple(BetaPrior(1, 1) for i in range(len(bandit_instance)))
    for i in range(horizon):
        action = policy.select_action(prior)
        p = bandit_instance[action]
        if bernoulli_pull(p):
            reward += 1
            prior = construct_posterior(prior, action, (1, 0))
            # print(f"{i}: Pulling {action} : 1")
        else:
            prior = construct_posterior(prior, action, (0, 1))
            # print(f"{i}: Pulling {action} : 0")
    return reward, regret_comp - reward


if __name__ == "__main__":
    NUM_ACTIONS = 2
    HORIZON = 30

    # tree = BayesianTree(t=HORIZON, k=NUM_ACTIONS)
    tree = StochasticBayesianTree(t=HORIZON, k=NUM_ACTIONS)
    
    for _ in range(50):
        reward, regret = run_bandit([0.9, 0.1], HORIZON, tree)
        print(f"Reward: {reward}  Regret: {regret}")
