import copy
from typing import List, Dict, Tuple

from bayesian_util import BetaPrior, construct_posterior


class BetaNode:
    def __init__(self, priors: Tuple[BetaPrior], depth: int) -> None:
        self.priors: List[BetaPrior] = priors;
        # Edges are defined by tuple (Action, Reward (= 0/1)) -> BetaNode
        self.edges: List[Action] = []
        self.depth = depth

        self.value = None
        self.best_action = None

    def get_value(self) -> float:
        if self.value is None:
            self._calc_value()
        return self.value

    def _calc_value(self) -> None:
        if len(self.edges) == 0:
            self.value = 0
            self.best_action = -1
        else:
            vals = [a.get_value() for a in self.edges]
            self.value, self.best_action = max(zip(vals, range(len(vals))))

    def __eq__(self, o: object) -> bool:
        return (o is BetaNode) and self.priors == o.priors

    def __hash__(self) -> int:
        return hash(self.priors)

    def __repr__(self) -> str:
        return str(self.priors)

class Action:
    def __init__(self, prior_node, action_taken, gain_reward_posterior, no_reward_posterior) -> None:
        self.prior_node: BetaNode = prior_node
        self.action_taken: int = action_taken
        self.gain_reward_posterior: BetaNode = gain_reward_posterior
        self.no_reward_posterior: BetaNode = no_reward_posterior

        self.value = None

    def get_value(self) -> float:
        if self.value is None:
            self._calc_value()
        return self.value

    def _calc_value(self) -> None:
        selected_prior = self.prior_node.priors[self.action_taken]
        p_reward = selected_prior.alpha / (selected_prior.alpha + selected_prior.beta)
        self.value = (p_reward * (1 + self.gain_reward_posterior.get_value())) + ((1-p_reward) * self.no_reward_posterior.get_value())


class BayesianTree:
    def __init__(self, t, k) -> None:
        self.t = t
        self.k = k

        self.nodes: Dict[Tuple[BetaPrior], BetaNode] = {}
        self.create_tree()

    
    def select_action(self, prior: Tuple[BetaPrior]) -> int:
        if prior not in self.nodes:
            raise ValueError(f"could not find tree node for prior {prior}")
        return self.nodes[prior].best_action

    def get_probs(self, prior: Tuple[BetaPrior]) -> List[float]:
        if prior not in self.nodes:
            return []
        best = self.nodes[prior].best_action
        return [1 if i == best else 0 for i in range(len(prior))]

    def create_tree(self):
        start_node = BetaNode(tuple(BetaPrior(1, 1) for _ in range(self.k)), depth=0)
        self.nodes[start_node.priors] = start_node

        open = [start_node]

        while len(open) > 0:
            parent = open.pop()
            for a in range(self.k):

                # Create posterior for the "gain reward" path
                c1 = construct_posterior(parent.priors, a, (1, 0))

                # Same as above, but for the "no reward" path
                c2 = construct_posterior(parent.priors, a, (0, 1))
                
                # Add states to open list if necessary
                # Check for pre-existing node with this prior
                if c1 not in self.nodes:
                    self.nodes[c1] = BetaNode(c1, parent.depth + 1)
                    if (parent.depth + 1) < self.t:
                        open.append(self.nodes[c1])
                if c2 not in self.nodes:
                    self.nodes[c2] = BetaNode(c2, parent.depth + 1)
                    if (parent.depth + 1) < self.t:
                        open.append(self.nodes[c2])
                
                # Add the action to the parent node
                parent.edges.append(Action(parent, a, self.nodes[c1], self.nodes[c2]))

        # Recursively calculate all node values and best actions
        start_node.get_value()
