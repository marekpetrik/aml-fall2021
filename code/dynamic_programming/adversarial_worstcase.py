import copy

from adversarial_instance import AdversarialInstance, generate_all_instances
from dynamic_bruteforce import BayesianTree
from stochastic_bruteforce import StochasticBayesianTree
from bayesian_util import BetaPrior, construct_posterior


# For testing purposes, always select arm 0
class NaiveAlgo:
    def select_action(self, _):
        return 0


def run_bandit(bandit_instance: AdversarialInstance, arms: int, horizon: int, policy):
    reward = 0
    regret_comp = bandit_instance.regret_comp
    prior = tuple(BetaPrior(1, 1) for i in range(arms))
    for i in range(horizon):
        action = policy.select_action(prior)
        if bandit_instance.rewards[action][i] == 1:
            reward += 1
            prior = construct_posterior(prior, action, (1, 0))
            # print(f"{i}: Pulling {action} : 1")
        else:
            prior = construct_posterior(prior, action, (0, 1))
            # print(f"{i}: Pulling {action} : 0")
    return reward, regret_comp - reward


ARMS = 2
HORIZON = 3

tree = BayesianTree(t=HORIZON, k=ARMS)
# tree = StochasticBayesianTree(t=HORIZON, k=ARMS)
# tree = NaiveAlgo()

worst_instance = ()
worst_regret = 0

for instance in generate_all_instances(arms=ARMS, horizon=HORIZON):
    reward, regret = run_bandit(instance, ARMS, HORIZON, tree)
    # print(f"{instance}: {regret}")
    if regret > worst_regret:
        worst_regret = regret
        worst_instance = (copy.deepcopy(instance.rewards), reward, regret)

print(f"Worst Instance: {worst_instance[0]}")
print(f"Worst Regret: {worst_regret}")
print(f"Associated Reward: {worst_instance[1]}")
