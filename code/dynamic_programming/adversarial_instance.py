from typing import List

class AdversarialInstance:
    def __init__(self, rewards: List[List[int]]) -> None:
        self.rewards = rewards
        # Regret comparison defined as selecting a single best arm
        self.regret_comp = max([sum(rs) for rs in self.rewards])

    def pull(self, arm: int, t: int):
        return self.rewards[arm][t]

    def __repr__(self) -> str:
        return self.rewards.__repr__()


def generate_all_instances(arms: int, horizon: int):
    # Produce a generator that iterates through all combinations of 0/1 rewards
    # 2^(k*t) possibilities !! maybe restrict this?

    # Start by constructing the environment itself:
    env = [[0 for _ in range(horizon)] for _ in range(arms)]

    def _dfs(env, a, h):
        # Base case:
        if a == arms-1 and h == horizon-1:
            env[a][h] = 0
            yield AdversarialInstance(env)
            env[a][h] = 1
            yield AdversarialInstance(env)
        # Recursive case:
        else:
            next_h = (h + 1) % horizon
            next_a = a + (1 if h + 1 == horizon else 0)
            env[a][h] = 0
            yield from _dfs(env, next_a, next_h)
            env[a][h] = 1
            yield from _dfs(env, next_a, next_h)

    return _dfs(env, 0, 0)


# Test all instance generation
if __name__ == '__main__':
    test_a = 2
    test_h = 3
    for instance in generate_all_instances(test_a, test_h):
        print(instance)