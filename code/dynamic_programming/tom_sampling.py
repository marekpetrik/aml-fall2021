from typing import List
import math

from bayesian_util import BetaPrior


def _softmax(elems: List[float]) -> List[float]:
    norm = sum([math.e ** elem for elem in elems])
    return [math.e ** elem / norm for elem in elems]


class TomSampler:
    def get_probs(self, prior: List[BetaPrior]):
        thetas = [p.alpha / (p.alpha + p.beta) for p in prior]
        scaled = _softmax(thetas)
        return scaled
