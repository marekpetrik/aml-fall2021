from typing import Tuple
from collections import namedtuple


BetaPrior = namedtuple("BetaPrior", ['alpha', 'beta'])


def construct_posterior(priors: Tuple[BetaPrior], action: int, prior_delta: Tuple[int]) -> Tuple[BetaPrior]:
    return tuple(
        p if i != action else BetaPrior(p.alpha + prior_delta[0], p.beta + prior_delta[1]) 
        for i, p in enumerate(priors)
    )