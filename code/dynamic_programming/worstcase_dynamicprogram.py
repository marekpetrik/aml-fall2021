from typing import List

from bayesian_util import BetaPrior, construct_posterior


def _generate_rewards(arms: int):
    def _dfs(state, i):
        if i == len(state):
            yield tuple(state)
        else:
            state[i] = 0
            yield from _dfs(state, i+1)
            state[i] = 1
            yield from _dfs(state, i+1)
    
    state = [0 for _ in range(arms)]
    return _dfs(state, 0)


def _immediate_regret(rewards: List[int], selected_arm: int):
    """Assumes that arm 0 is optimal"""
    return rewards[0] - rewards[selected_arm]


class AdversarialDynamicProgram:
    def __init__(self) -> None:
        self.memo = {}

    def expected_value(self, tree, arms: int, horizon: int):
        """Calculate the reactive expected worst-case value for a stochastic agent"""
        starting_prior = tuple(BetaPrior(1, 1) for _ in range(arms))
        return self._expect(tree, starting_prior, arms, horizon, 0)

    def _expect(self, tree, prior, arms: int, horizon: int, depth: int):
        if prior not in self.memo:
            probs = tree.get_probs(prior)
            if not probs or depth >= horizon:
                self.memo[prior] = 0
            else:
                all_futures = [self._regret(tree, prior, probs, reward, arms, horizon, depth) for reward in _generate_rewards(arms)]
                self.memo[prior] = max(all_futures)
        return self.memo[prior]

    def _regret(self, tree, prior, probs, rewards, arms: int, horizon: int, depth: int):
        """The value of a node is equal to the weighted sum of the actions
        The value of an action is equal to the future worst-case regret, 
        plus the immediate regret from choosing that action.
        """
        return sum([
            probs[i] * 
            (self._expect(tree, construct_posterior(prior, i, (1, 0) if rewards[i] == 1 else (0, 1)), arms, horizon, depth+1) 
            + _immediate_regret(rewards, i))
            for i in range(arms)
        ])


if __name__ == '__main__':
    for r in _generate_rewards(2):
        print(r)
